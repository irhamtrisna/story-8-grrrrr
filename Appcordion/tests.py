from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import unittest

# Create your tests here.
class UnitTest(TestCase):
    def test_accordion_url_is_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_accordion_using_index_func(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, index)

    def test_accordion_using_main_html(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'main.html')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/accordion')
        super(FunctionalTest, self).setUp()

    def test_if_accordion_can_expand_and_collapse(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)
        accordion = selenium.find_element_by_name('accordion-1')

        accordion.click()
        activeAccordion = accordion.get_attribute("class")
        # self.assertNotEqual(activeAccordion.find("active"), -1)

        accordion.click()
        inactiveAccordion = accordion.get_attribute("class")
        self.assertEqual(inactiveAccordion.find("active"), -1)

    def test_for_accordion_content(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)
        self.assertIn('Document', selenium.title)

        accordion1 = selenium.find_element_by_name('accordion-1')
        accordion2 = selenium.find_element_by_name('accordion-2')
        accordion3 = selenium.find_element_by_name('accordion-3')
        accordion4 = selenium.find_element_by_name('accordion-4')

        accordion1.click()
        self.assertIn('Irham Trisna Ananto', selenium.page_source)
        accordion1.click()

        accordion2.click()
        self.assertIn('Gita Bahana Nusantara', selenium.page_source)
        accordion2.click()

        accordion3.click()
        self.assertIn('Tomorrow', selenium.page_source)
        accordion3.click()

        accordion4.click()
        self.assertIn('Contact', selenium.page_source)
        accordion4.click()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()