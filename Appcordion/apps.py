from django.apps import AppConfig


class AppcordionConfig(AppConfig):
    name = 'Appcordion'
