from django.urls import path
from . import views

app_name = 'Appcordion'

urlpatterns = [
    path('accordion/', views.index, name='index'),
]